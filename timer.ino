/*School project: "UV exposure unit for PCB" */

#include <EEPROM.h>

#define shortBeep 200
#define longBeep 500
#define buttonInc A1
#define buttonDec A2
#define buttonControl A3
#define ledColon A4
#define transistorSwitch A5
#define buzzer 9
#define delayButton 300
//common pins are 10,11,12,13

unsigned long timeCheck;
int seconds = 0;
int minutes = 0;
bool timeTicking = false;
bool ledState;
bool saveLast = true;
bool heldDown = false;

/*1 represents segments turned ON, 0 represents segments turned OFF*/
bool numbers[10][7] = {
  {1, 1, 1, 1, 1, 1, 0},
  {0, 1, 1, 0, 0, 0, 0},
  {1, 1, 0, 1, 1, 0, 1},
  {1, 1, 1, 1, 0, 0, 1},
  {0, 1, 1, 0, 0, 1, 1},
  {1, 0, 1, 1, 0, 1, 1},
  {1, 0, 1, 1, 1, 1, 1},
  {1, 1, 1, 0, 0, 0, 0},
  {1, 1, 1, 1, 1, 1, 1},
  {1, 1, 1, 0, 0, 1, 1}
};


/*reset by going to address 0*/
void(* resetFunc) (void) = 0; 

/*for each digit for each segment set the state of segments according to the numbers table*/
void showNumber(int numberInput) {
  int pos = 1000;
  for (byte i = 0; i < 4; i++) {
    int digit = numberInput / pos;
    digitalWrite(10 + i, LOW);
    for (byte j = 0; j < 7; j++) {
      digitalWrite(2 + j, numbers[digit][j]);
    }
    delayMicroseconds(4500);
    digitalWrite(10 + i, HIGH);
    numberInput = numberInput - (digit * pos);
    pos = pos / 10;
  }
}

void timeInc() {
  if (seconds < 30) {
    seconds = 30;
  }
  else if ((seconds >= 30) & (minutes < 99)) {
    minutes++;
    seconds = 0;
  }
}

void timeDec() {
  if (seconds > 30) {
    seconds = 30;
  }
  else if ((seconds <= 30) & (seconds > 0)) {
    seconds = 0;
  }
  else if (seconds == 0) {
    minutes--;
    if (minutes == -1) {
      minutes = 0;
      seconds = 0;
    }
    else {
      seconds = 30;
    }
  }
}

void beep(int duration) {
  digitalWrite(ledColon, HIGH);
  digitalWrite(buzzer, HIGH);
  unsigned long beepTimeCheck = millis();
  while (millis() - beepTimeCheck <= duration) {
    showNumber(minutes * 100 + seconds);
  }
  digitalWrite(buzzer, LOW);
  digitalWrite(ledColon, LOW);
  ledState = false;
}

void setup() {
  pinMode(buttonInc, INPUT);
  pinMode(buttonDec, INPUT);
  pinMode(buttonControl, INPUT);
  pinMode(ledColon, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(transistorSwitch, OUTPUT);
  for (byte i = 2; i < 14; i++) {
    pinMode(i, OUTPUT);
  }
  minutes = EEPROM.read(0);
  seconds = EEPROM.read(1);
}

void loop() {
  /*SETUP state*/
  if (timeTicking == false) {
    ledState = true;
    
    /*INCREASE wanted time*/
    if (digitalRead(buttonInc) == HIGH) {
      timeInc();
      timeCheck = millis();
      while (digitalRead(buttonInc) == HIGH) {
        if (millis() - timeCheck >= delayButton) {
          timeInc();
          timeCheck = millis();
        }
        showNumber(minutes * 100 + seconds);
      }
    }
    
    /*DECREASE wanted time*/
    if (digitalRead(buttonDec) == HIGH) {
      timeDec();
      timeCheck = millis();
      while (digitalRead(buttonDec) == HIGH) {
        if (millis() - timeCheck >= delayButton) {
          timeDec();
          timeCheck = millis();
        }
        showNumber(minutes * 100 + seconds);
      }
    }
    
    /*CONTROL button handler*/
    if ((digitalRead(buttonControl) == HIGH) & ((minutes + seconds) > 0)) {
      unsigned long stopTimeCheck = millis();
      while (digitalRead(buttonControl) == HIGH) {
        showNumber(minutes * 100 + seconds);
        /*set wanted time to zero if button is held down*/
        if ((millis() - stopTimeCheck >= 1500) & (heldDown == false)) {
          minutes = 0;
          seconds = 0;
          heldDown = true;
          beep(longBeep);
        }
      }
      if ((minutes + seconds) > 0) {
        /*save wanted values to EEPROM*/
        if (saveLast == true) {
          EEPROM.update(0, minutes);
          EEPROM.update(1, seconds);
          saveLast = false;
        }
        beep(shortBeep);
        if (seconds > 0) {
          seconds--;
        }
        else {
          minutes--;
          seconds = 59;
        }
        timeTicking = true;
        timeCheck = millis();
        digitalWrite(transistorSwitch, HIGH);
      }
    }
    else if ((digitalRead(buttonControl) == HIGH) & ((minutes + seconds) == 0)) {
      unsigned long stopTimeCheck = millis();
      while (digitalRead(buttonControl) == HIGH) {
        showNumber(minutes * 100 + seconds);
        if ((millis() - stopTimeCheck >= 1500) & (heldDown == false)) {
          minutes = EEPROM.read(0);
          seconds = EEPROM.read(1);
          heldDown = true;
          beep(longBeep);
          saveLast = true;
        }
      }
    }
    heldDown = false;
  }
  
  /*TIME RUNNING state*/
  if (timeTicking == true) {
    
    /*Stopped by control button*/
    if ((digitalRead(buttonControl) == HIGH) & (minutes + seconds > 0) ) {
      timeTicking = false;
      ledState = true;
      digitalWrite(transistorSwitch, LOW);
      beep(shortBeep);
      while (digitalRead(buttonControl) == HIGH) {
        showNumber(minutes * 100 + seconds);
        digitalWrite(ledColon, HIGH);
      }
    }
    else if (millis() - timeCheck >= 1000) {  
      seconds--;
      ledState = !ledState;
      timeCheck = millis();
      if (seconds == -1) {
        if (minutes > 0) {
          minutes--;
          seconds = 59;
        }
        
        /*Timer ends*/
        else {
          seconds = 0;
          digitalWrite(transistorSwitch, LOW);
          beep(shortBeep);
          unsigned long buzzerOffTimeCheck = millis();
          while (millis() - buzzerOffTimeCheck <= shortBeep) {
            showNumber(0);
          }
          beep(shortBeep);
          buzzerOffTimeCheck = millis();
          while (millis() - buzzerOffTimeCheck <= shortBeep) {
            showNumber(0);
          }
          beep(longBeep);
          resetFunc();
        }
      }
    }
  }
  
  /*Display the number, set the LED state according to ledState*/
  showNumber(minutes * 100 + seconds);
  digitalWrite(ledColon, ledState);
}
